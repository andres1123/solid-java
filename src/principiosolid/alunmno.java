package principiosolid;

public class alunmno extends persona {

    public String codigoEstud;
    public double notaMateria1;
    public double notaMateria2;

    public alunmno( String cedula, String nombre, String codigoEstud, double notaFinal,double notaParcial) {
        super(cedula, nombre);
        this.codigoEstud = codigoEstud;
        this.notaMateria1 = notaFinal;
        this.notaMateria2=notaParcial;
    }
    public double Promedio(){
         double promedio = ((notaMateria1+notaMateria2)/2);
       return promedio;
    }
    
 

}
